import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-users-profile',
  templateUrl: './users-profile.component.html',
  styleUrls: ['./users-profile.component.css']
})

export class UsersProfileComponent implements OnInit {
  firstName: string;
  lastName: string;
  city: string;
  country: string;
  sex: string;
  temperature: number;
  humidity: number;

  constructor(private matDialogRef: MatDialogRef<UsersProfileComponent>, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.firstName = this.data.firstName;
    this.lastName = this.data.lastName;
    this.city = this.data.city;
    this.country = this.data.country;
    this.sex = this.data.sex;
  }

  closeModal(){
    this.matDialogRef.close();
  }
}
