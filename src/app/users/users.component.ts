import { Component} from '@angular/core';
import { UsersService } from '../shared/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {
  submitted: boolean;
  successInfo: boolean;
  formControls = this.usersService.form.controls;

  constructor(private usersService: UsersService) { }  

  onSubmit(){
    this.submitted = true;
    if(this.usersService.form.valid){
      if(this.usersService.form.get('$id').value == null)
        this.usersService.addUser(this.usersService.form.value);
      else
        this.usersService.updateUser(this.usersService.form.value);
      this.successInfo = true;
      setTimeout(() => this.successInfo = false, 3000);
      this.submitted = false;
      this.usersService.form.reset();
    }
  }
}
