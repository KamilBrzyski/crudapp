import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  usersList: AngularFireList<any>;
  constructor(private firebase: AngularFireDatabase) { }

  form = new FormGroup({
    $id: new FormControl(null),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    sex: new FormControl('', Validators.required)
  });

  getUsers(){
    this.usersList = this.firebase.list('users');
    return this.usersList.snapshotChanges();
  }

  addUser(users){
    this.usersList.push({
      firstName:users.firstName,
      lastName:users.lastName,
      city:users.city,
      country:users.country,
      sex:users.sex
    });
  }

  populateForm(users){
    this.form.setValue(users);
  }

  updateUser(users){
    this.usersList.update(users.$id,
      {
        firstName: users.firstName,
        lastName: users.lastName,
        city: users.city,
        country: users.country,
        sex: users.sex
      });
  }

  deleteUser($id: string){
    this.usersList.remove($id);
  }

}