import { HttpClient } from '@angular/common/http';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { map } from 'rxjs/operators';

export interface WeatherInfo {
  query: {
    results: {
      channel: {
        atmosphere: {
          humidity: number;
        }
        item: {
          condition: {
            temp: number;
          }
        }
      }
    }
  }
}

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnChanges {
  @Input() userCity: string;
  @Input() userCountry: string;
  temp: number;
  humidity: number;

  constructor(private _http: HttpClient) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.userCity !== null && changes.userCity.currentValue !== null
      && changes.userCountry != null && changes.userCountry.currentValue !== null) {
      this.getUserWeather(changes.userCity.currentValue, changes.userCountry.currentValue);
    }
  }

  getUserWeather(city, country) {
    const locationQuery =
      escape('select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' + city + ', ' + country + '"');
    const url =
      'https://query.yahooapis.com/v1/public/yql?q=' + locationQuery + ')%20and%20u%3D"c"&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys';

    this._http.get(url)
      .pipe(map((result: WeatherInfo) => result))
      .subscribe((result) => {
        this.temp = result.query.results.channel.item.condition.temp;
        this.humidity = result.query.results.channel.atmosphere.humidity;
    });
  }
}
