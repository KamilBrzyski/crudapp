import { Component, OnInit } from '@angular/core';
import { UsersService } from '../shared/users.service';
import { MatDialog } from '@angular/material';
import { UsersProfileComponent } from '../users-profile/users-profile.component';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  usersArray = [];
  isDeleted: boolean;

  constructor(private usersService: UsersService, public dialog: MatDialog) { }

  ngOnInit() {
    this.usersService.getUsers().subscribe(
      list => {
        this.usersArray = list.map(item => {
          return{
            $id: item.key,
            ...item.payload.val()
          };
        });
    });
  }

  onDelete($id){
    if(confirm('Are you sure to delete this record?')){
      this.usersService.deleteUser($id);
      this.isDeleted = true;
      setTimeout(() => this.isDeleted = false, 3000);
    }
  }

  openModal(user){
    this.dialog.open(UsersProfileComponent, {
      width: '280px',
      data:user
    });
  }

  closeModal(){
    //this.dialogRef.close();
  }

}
